import { useForm } from "react-hook-form";
import React, { useContext } from 'react';
import {LanguageContext} from '../GameSeries/LanguageContext';

    export function Contact () {

        const { register, handleSubmit, watch, errors } = useForm();
        const onSubmit = data => console.log(data);
        
        const Language = useContext(LanguageContext);
      
        return (
          
          <form onSubmit={handleSubmit(onSubmit)}>
              
            <label name="name">Name 
            <input name="name" defaultValue="name" ref={register} />
            </label>
            <label name="Email">Email 
            <input name="Email" defaultValue="Email" ref={register} />
            </label>
            <label name="FavoriteAmiibo">FavoriteAmiibo 
            <input name="FavoriteAmiibo" defaultValue="FavoriteAmiibo" ref={register} />
            </label>
            <label name="message">Message 
            <input name="message" defaultValue="" ref={register} />
            </label>
            {Language}
          </form>
        );
    
    
};
