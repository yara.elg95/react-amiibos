import React, { useReducer } from 'react';
import './CounterWithUserReducer.scss';
import { useParams } from 'react-router-dom'
const initialState = { count: 0 };
function reducer (state, action) {
    switch (action.type) {
        case 'increment':
            return { count: state.count + 1 };
        case 'decrement':
            return { count: state.count - 1 };
        case 'reset':
            return { count: state = 0 };
        default:
            throw new Error();
    }
}
export const CounterWithUseReducer = function () {
    const initialValue = Number(useParams().initialValue);
    const [state, dispatch] = useReducer(reducer, initialState);
    return (
        <div>
            <p>Count: {state.count}</p>
            <button className="b-counter-with-hooks__button" onClick={() => dispatch({ type: 'decrement' })}>-</button>
            <button className="b-counter-with-hooks__button" onClick={() => dispatch({ type: 'increment' })}>+</button>
            <button className="b-counter-with-hooks__button" onClick={() => dispatch({ type: 'reset' })}>0 </button>
        </div>
    );
}