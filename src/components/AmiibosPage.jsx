import React, {useContext, useEffect, useState} from "react";
import axios from "axios";

export function AmiibosPage(props) {
    const [amibotArray, setAmibotArray] = useState("");
    const cadaUnoDeLosAmiiBoo = [];
// Make a request for a user with a given ID
    useEffect (() => {
    // PASO 2
    axios.get('https://www.amiiboapi.com/api/amiibo/')
        .then(function (response) {
            // handle success
            // SETEAR LA VARIABLE PARA TRABAJAR CON ELLA
            setAmibotArray(response.data.amiibo);
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .then(function () {
            // always executed
        })}, []);

        for (let i = 0; i < amibotArray.length; i++) {
             cadaUnoDeLosAmiiBoo.push(<div key={i} className="col-sm-3 caja">  
             <h5>{amibotArray[i].amiiboSeries}</h5> 
             <img src={amibotArray[i].image} alt="nada" className="image"/> 
            <h2>{amibotArray[i].name}</h2 > </div>
             );
        }
    return (
        // Make a request for a user with a given ID
        <>
        

         <div className="container">
             <div className = "row">
             {cadaUnoDeLosAmiiBoo}
             </div>
             </div>
         </>
    )
};