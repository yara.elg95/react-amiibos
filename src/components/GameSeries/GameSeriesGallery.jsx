import React from "react";
export function GameSeriesGallery(props) {
    return (
        <div>
            <div className="row">
                {props.gameSeries.map((item, index) =>
                    <div key={index} className="col-md-3">
                        <p>{item.name}</p>
                    </div>
                )}
            </div>
        </div>
    )
}
