import React, {useEffect, useState} from 'react';
import axios from "axios";
import {GameSeriesGallery} from "./GameSeriesGallery";

export function GameSeries () {
    const [gameSeries, setGameSeries] = useState([]);
    useEffect(()=> {
        axios.get('https://www.amiiboapi.com/api/gameseries/')
            .then(function (response) {
                // handle success
                const gameSeries = response.data.amiibo;
                setGameSeries(gameSeries);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
        console.log("probando use effect")
    },[])
    return (
        <GameSeriesGallery gameSeries={gameSeries}/>
    )
};