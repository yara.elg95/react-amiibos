import React from "react";
import styled from "styled-components";


export function AmiibosGallery(props) {

    const FigcaptionLocalStyles = styled.figcaption`
        background-color: ${({ theme }) => theme.backgroundOpacity};
    }`;
    
    return (
        <div>
            <div className="row">
                {props.amiibos.map((item, index) =>
                    <div key={index} className="col-md-3">
                        <img src={item.image} alt="" />
                        <p></p>
                        <FigcaptionLocalStyles
                            className="c-amiibo-gallery__caption">{item.name}</FigcaptionLocalStyles>
                        <p></p>
                    </div>
                )}
            </div>
        </div>
    )
}