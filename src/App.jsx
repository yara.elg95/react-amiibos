import React from 'react';
import logo from './logo.svg';
import './App.scss';
import { Home } from './components/Home';
import { Amiibos } from './components/Amiibos';
import { GameSeries} from './components/GameSeries/GameSeries';
import { Contact } from './components/Contact/Contact';
import {BrowserRouter as Router, Switch, Route, Link, useRouteMatch,
  useParams} from "react-router-dom";
import { AmiibosPage } from './components/AmiibosPage';
import {LanguageContext} from './components/GameSeries/LanguageContext';
import { ThemeProvider } from 'styled-components';
import { useDarkMode } from './shared/hooks/useDarkMode';
import {lightTheme, darkTheme} from './shared/styles/Themes'


export default function App() {
  const [theme, themeToggler] = useDarkMode();
  const themeMode = theme ==='light'? lightTheme : darkTheme;
  
  return (
     
    <LanguageContext.Provider value={'en'}>
      <Router>
        <ThemeProvider theme={themeMode}>
          <button onClick={themeToggler}>Cambiar Tema</button>
        <div>
          <nav>
            <ul className="b-list">  
              <li className="b-list__list">
                <Link className="b-list__list" to="/">Home</Link>
              </li>
              <li className="b-list__list">
                <Link className="b-list__list" to="Amiibos">Amiibos</Link>
              </li>
              <li className="b-list__list">
                <Link className="b-list__list" to="/GameSeries">GameSeries</Link>
              </li>
              <li className="b-list__list">
                <Link className="b-list__list" to="/Contact">Contact</Link>
              </li>
              <li className="b-list__list">
                <Link className="b-list__list" to="/AmiibosPage">AmiibosPage</Link>
              </li>
              <li className="b-list__list">
                <Link className="b-list__list" to="/CounterWithUserReducer">CounterWithUserReducer</Link>
              </li>  
            </ul>
          </nav>
          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
          <Route path="/AmiibosGallery">
              <AmiibosPage/>
            </Route>
          <Route path="/AmiibosPage">
              <AmiibosPage/>
            </Route>
            <Route path="/Amiibos">
              <Amiibos/>
            </Route>
            <Route path="/GameSeries">
              <GameSeries/>
            </Route>
            <Route path="/Contact">
              <Contact/>
            </Route>
            <Route path="/">
              <Home/>
            </Route>
          </Switch>
        </div>
        </ThemeProvider>
      </Router>
      </LanguageContext.Provider>
  );
}


